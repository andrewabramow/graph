/*
� ����� ��������� ��������� ����������, ��� ����� ����� �
���������� ���������� � ��� ���� ����� ����������� �
��������� (��� ����� ������� ��������� � ������ ���������)

��� ��� ������� ��������� ��� ������������� ����������������
�����

���������� �������� ��� ���������� ����������:

    ListGraph, �������� ���� � ���� ������� ������� ���������,
    MatrixGraph, �������� ���� � ���� ������� ���������,

����� ���������� ����������� �����������, ����������� IGraph*.
����� ����������� ������ ����������� ���������� ���� �
����������� ������ (�������� ��������, ��� ������ � ����
���������� ����� ���������� ������) ���������� � ��� �����
��� ������������ ����� � ��������� ������������, ����
����������.
*/

#include <iostream>
#include <vector>
#include "IGraph.h"
#include "MatrixGraph.h"
#include "ListGraph.h"

int main()
{
    //ListGraph* list = new ListGraph();
    //list->AddEdge(1, 2);
    //list->AddEdge(1, 3);
    //list->AddEdge(2, 4);
    //list->AddEdge(3, 5);
    //list->AddEdge(5, 6);

    //std::vector<int> vertices_next;
    //list->GetNextVertices(1, vertices_next);
    //for (int i = 0; i < vertices_next.size(); ++i) {
    //    std::cout << vertices_next[i] << std::endl;
    //}

    //std::vector<int> vertices_prev;
    //list->GetPrevVertices(6, vertices_prev);
    //for (int i = 0; i < vertices_prev.size(); ++i) {
    //    std::cout << vertices_prev[i] << std::endl;
    //}

    MatrixGraph* sptr = new MatrixGraph (5);
    sptr->AddEdge(0, 1);
    sptr->AddEdge(1, 2);
    sptr->AddEdge(2, 3);
    sptr->AddEdge(3, 4);


    ListGraph* new_list = new ListGraph(sptr);

    /*std::vector<int> vertices2;
    sptr->GetNextVertices(2, vertices2);
    for (int i = 0; i < vertices2.size(); ++i) {
        std::cout << vertices2[i] << std::endl;
    }*/

}


