#include <iostream>
#include <vector>
#include "MatrixGraph.h"

void MatrixGraph::AddEdge(int from, int to) {

    matrix[from][to] = true;
    std::cout << "new edge: " << from << " to " << to << " added\n";

    for (int i = 0; i < n; ++i) { // output result
        for (int j = 0; j < n; ++j) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}
int MatrixGraph::VerticesCount() {
    return matrix.size();
}
void MatrixGraph::GetPrevVertices(int vertex, std::vector<int>& vertices){
    for (int i = 0; i < n; ++i) {
        if (matrix[i][vertex] == true) {
            vertices.push_back(i);
            GetPrevVertices(i, vertices);
        }
    }
}
void MatrixGraph::GetNextVertices(int vertex, std::vector<int>& vertices) {
    for (int i = 0; i < n; ++i) {
        if (matrix[vertex][i] == true) {
            vertices.push_back(i);
            GetNextVertices(i, vertices);
        }  
    }
}
std::vector<std::vector<bool>> MatrixGraph::get_cont() {
    return matrix;
}
