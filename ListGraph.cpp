
#include <iostream>
#include <vector>
#include "ListGraph.h"


void ListGraph::AddEdge(int from, int to){
	adj_list[from].push_back(to);
}

int ListGraph::VerticesCount() {
	return adj_list.size();
}

void ListGraph::GetNextVertices(int vertex, std::vector<int>& vertices) {
	if (vertex > 0) {
		for (int i = 0; i < adj_list[vertex].size(); ++i) {
			vertices.push_back(adj_list[vertex][i]);
			GetNextVertices(adj_list[vertex][i], vertices);
		}
	}
}
void ListGraph::GetPrevVertices(int vertex, std::vector<int>& vertices) {
	if (vertex > 0) {
		for (int i = 0; i < adj_list.size(); ++i) {
			for (int j = 0; j < adj_list[i].size(); ++j) {
				if (adj_list[i][j] == vertex) {
					vertices.push_back(i);
					GetPrevVertices(i, vertices);
				}
			}
		}
	}
}
std::map <int, std::vector<int>> ListGraph::get_cont() {
	return adj_list;
}