#pragma once
#include <iostream>
#include <map>
#include <vector>
#include "IGraph.h"
#include "MatrixGraph.h"

class ListGraph : public IGraph {
private:
    std::map <int, std::vector<int>> adj_list;
public:
    ListGraph() {}
    ListGraph(IGraph* _oth) {
        ListGraph(this);
    }
    ListGraph(ListGraph* _oth) {
        adj_list = _oth -> adj_list;
    }
    ListGraph(MatrixGraph* _oth) {
        adj_list.clear();
        for (int i = 0; i < _oth->get_cont().size(); ++i) {
            for (int j = 0; j < _oth->get_cont()[i].size(); ++j) {
                if (_oth->get_cont()[i][j]) {
                    adj_list[i].push_back(j);
                }
            }
        }
    }
    ListGraph& operator= (const ListGraph& oth_list) {
        adj_list.clear();
        adj_list = oth_list.adj_list;
        return *this;
    }
    void AddEdge(int from, int to) override;
    // ����� ��������� ������� ������ � ����� ����� � ��������� �����
    int VerticesCount() override;
    // ����� ������ ������� ������� ���������� ������
    void GetNextVertices(int vertex, std::vector<int>& vertices) override;
    // ��� ���������� ������� ����� ������� � ������ ��������� ��� �������, � ������� ����� ����� �� ����� �� ������
    void GetPrevVertices(int vertex, std::vector<int>& vertices) override;
    // ��� ���������� ������� ����� ������� � ������ ��������� ��� �������, �� ������� ����� ����� �� ����� � ������
    std::map <int, std::vector<int>>get_cont();
};