#pragma once
#include <vector>
#include "IGraph.h"
#include "ListGraph.h"
class MatrixGraph : public IGraph {
private:
    std::vector<std::vector<bool>>matrix;
    int n = 0;
public:
    MatrixGraph(int dim) { // dimensions
        n = dim;
        matrix.resize(dim);
        for (int i = 0; i < dim; ++i) {
            matrix[i].resize(dim);
        }
        for (int i = 0; i < dim; ++i) {
            matrix.push_back(std::vector<bool>());
            for (int j = 0; j < dim; ++j) {
                matrix[i].push_back(false);
            }
        }
    }
    MatrixGraph(IGraph* _oth) {
        MatrixGraph(this);
    }
    MatrixGraph(MatrixGraph* _oth) {
        matrix = _oth->matrix;
        n = _oth->n;
    }
    MatrixGraph(ListGraph* _oth) {
        matrix.clear(); // new matrix
        n = _oth->get_cont().size();
        matrix.resize(n);
        for (int i = 0; i < n; ++i) {
            matrix[i].resize(n);
        }
        for (int i = 0; i < n; ++i) {
            matrix.push_back(std::vector<bool>());
            for (int j = 0; j < n; ++j) {
                matrix[i].push_back(false);
            }
        }
        // copy from ListGraph
        std::map<int, std::vector<int>>::iterator it;
        for (it = _oth->get_cont().begin();
            it != _oth->get_cont().end();
            ++it) {
            for (int i = 0; i < _oth->get_cont()[it->first].size(); ++i) {
                matrix[it->first][i] = true;
            }
        }
    }
    MatrixGraph& operator= (const MatrixGraph& oth_matrix) {
        matrix.clear();
        matrix = oth_matrix.matrix;
        return *this;
    }
    void AddEdge(int from, int to) override;
    int  VerticesCount() override;
    void GetNextVertices(int vertex, std::vector<int>& vertices) override;
    void GetPrevVertices(int vertex, std::vector<int>& vertices) override;
    std::vector<std::vector<bool>> get_cont();
    };
