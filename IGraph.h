#pragma once
#include <iostream>
#include <vector>

class IGraph {

public:

    virtual ~IGraph() {}
    IGraph() {};
    IGraph(IGraph* _oth) {};
    virtual void AddEdge(int from, int to);
    // ����� ��������� ������� ������ � ����� ����� � ��������� �����
    virtual int VerticesCount();
    // ����� ������ ������� ������� ���������� ������
    virtual void GetNextVertices(int vertex, std::vector<int>& vertices);
    // ��� ���������� ������� ����� ������� � ������ ��������� ��� �������, � ������� ����� ����� �� ����� �� ������
    virtual void GetPrevVertices(int vertex, std::vector<int>& vertices);
    // ��� ���������� ������� ����� ������� � ������ ��������� ��� �������, �� ������� ����� ����� �� ����� � ������
};